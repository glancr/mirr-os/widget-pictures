module Pictures
  class Engine < ::Rails::Engine
    isolate_namespace Pictures
    config.generators.api_only = true

    DEFAULT_STYLES = { horizontal_align: 'center' }.freeze
  end
end
