# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'pictures/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'pictures'
  s.version     = Pictures::VERSION
  s.authors     = ['Tobias Grasse']
  s.email       = ['tg@glancr.de']
  s.homepage    = 'https://glancr.de/modules/pictures'
  s.summary     = 'mirr.OS Widget that shows an uploaded image.'
  s.description = 'Upload one or more images to display on your mirror.'
  s.license     = 'MIT'
  s.metadata    = { 'json' =>
                  {
                    type: 'widgets',
                    title: {
                      enGb: 'Pictures',
                      deDe: 'Bilder',
                      frFr: 'Images',
                      esEs: 'Fotos',
                      plPl: 'Zdjęcia',
                      koKr: '영화'
                    },
                    description: {
                      enGb: s.description,
                      deDe: 'Lade ein oder mehrere Bilder hoch, um sie auf deinem Spiegel anzuzeigen.',
                      frFr: 'Téléchargez une ou plusieurs images à afficher sur votre miroir.',
                      esEs: 'Sube una o más imágenes para que se muestren en tu espejo.',
                      plPl: 'Prześlij jeden lub więcej obrazów, aby wyświetlić je na swoim lustrze.',
                      koKr: '하나 이상의 이미지를 업로드하여 미러에 표시하십시오.'
                    },
                    sizes: [
                      {
                        w: 6,
                        h: 4
                       },
                      {
                        w: 4,
                        h: 6
                      },
                      {
                        w: 12,
                        h: 8
                      },

                      {
                        w: 8,
                        h: 12
                      },
                      {
                        w: 8,
                        h: 6
                      },
                      {
                        w: 6,
                        h: 8
                      },
                      {
                        w: 12,
                        h: 10
                      },
                      {
                        w: 10,
                        h: 12
                      },
                      {
                        w: 12,
                        h: 21
                      },
                      {
                        w: 21,
                        h: 12
                      }
                    ],
                    languages: %i[enGb deDe frFr esEs plPl koKr],
                    group: nil
                  }.to_json
                }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails'
end
