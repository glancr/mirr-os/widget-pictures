# frozen_string_literal: true

Pictures::Engine.routes.draw do
  resources :picture_uploads
end
