# frozen_string_literal: true

module Pictures
  class Configuration < WidgetInstanceConfiguration
    attribute :picture_ids, :string, default: "[]"
      attribute :rotation_interval, :integer, default: 5
      attribute :remote_files, :boolean, default: false
      attribute :remote_picture_urls, :string, default: "[]"

    validates :remote_files, boolean: true
    # TODO: Refactor ID/URL storage to proper arrays.
    # Requires a refactor in the frontend and a migration, plus changed form handling.
  end
end
