module Pictures
  class PictureUpload < Upload
    validate :file_format

private

def file_format
  return unless file.attached?
  return if file.blob.content_type.start_with? 'image/'
  file.purge_later
  errors.add(:file, 'needs to be an image')
end
  end
end
